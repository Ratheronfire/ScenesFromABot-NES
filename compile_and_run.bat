if not exist "bin" mkdir bin
@del bin/whoseline.o
@del bin/whoseline.nes
@del bin/whoseline.map.txt
@del bin/whoseline.labels.txt
@del bin/whoseline.nes.ram.nl
@del bin/whoseline.nes.0.nl
@del bin/whoseline.nes.1.nl
@del bin/whoseline.nes.dbg

@echo.
@echo Building music file...
C:\Users\Rathe\Famitracker\FamiTracker.exe data\hoedown.ftm -export data\hoedown.txt
C:\Users\Rathe\famitone2\tools\text2data.exe -ca65 data\hoedown.txt

@echo.
@echo Compiling...

ca65.exe whoseline.s -g -o bin/whoseline.o
@IF ERRORLEVEL 1 GOTO failure

@echo.
@echo Linking...

ld65.exe -o bin/whoseline.nes -C whoseline.cfg bin/whoseline.o -m bin/whoseline.map.txt -Ln bin/whoseline.labels.txt --dbgfile bin/whoseline.dbg

@IF ERRORLEVEL 1 GOTO failure

@echo.
@echo Success!

C:\Users\Rathe\Downloads\Mesen.exe bin/whoseline.nes
@GOTO endbuild

:failure
@echo.
@echo Build error!

:endbuild
