Whose Line NES
==============

The 2021 April Fools project for [Scenes From a Bot](https://twitter.com/ScenesFromABot), my GPT-2 based Twitter bot inspired by Whose Line is it Anyway. This project produces an actual, working NES ROM displaying a pre-generated prompt, alongside a short animation and my rendition of the Hoedown theme.

Usage
-----

To change the prompt, simply overwrite the contents of prompt.txt. Text should be formatted in all caps, with no more than 28 characters per line, and no more than six lines. Valid characters include A-Z, 0-9, space, and the following special characters: ``!"#$%&'()*+,-./:;<=>?[\]^_`{|}~``

CA65 is used to compile the project into a .o file, and LD65 is used to build the NES ROM:

```bash
ca65.exe whoseline.s -g -o bin/whoseline.o

ld65.exe -o bin/whoseline.nes -C whoseline.cfg bin/whoseline.o -m bin/whoseline.map.txt -Ln bin/whoseline.labels.txt --dbgfile bin/whoseline.dbg
```

Design
------

The code for this project was hastily edited from tutorial code by [bbbradsmith](https://github.com/bbbradsmith/NES-ca65-example). This was a learning exercise in NES development, so my code is probably super inefficient, and there are several places where I massively over-complicated the design. Other tools used included [Sumez's](http://nesdev.eternal.dk/) online nametable editor to create the background art, [Famitracker](famitracker.com/) to produce the Hoedown music, and [Shiru's Famitone 2](http://shiru.untergrund.net/code.shtml) to incorporate the Famitracker music into the ROM. The Yamaha samples used in the music were provided by [Yung Gotenks](http://forums.famitracker.com/viewtopic.php?t=4520#p23109) on the Famitracker forums.

In addition, I created a bash script (compile_and_record.sh) to automatically prepare the ROM for posting to Twitter, which consisted of several steps:

* Pull the next entry queued for posting
* Convert it to uppercase and split into lines of 28 width, and save to prompt.txt
* Build the ROM
* Open [Retroarch](https://www.libretro.com/) and record 1500 frames (~25 seconds) of footage.
* Convert the output rom for Twitter
  * Extract the final frame and re-insert it at the start, so the thumbnail isn't just a blank screen.
  * Convert it to a compatible format (mp4, exact resolution, and a bunch of other settings)
* Save the output video and wait for the Twitter bot to pull it and post it

This script automatically runs every two hours, meaning that the Twitter bot is able to post the pre-generated recordings without any manual intervention.
