;
; pong.s
;
; Based on:
; 	https://github.com/bbbradsmith/NES-ca65-example

;
; iNES header
;

.segment "HEADER"

INES_MAPPER = 0 ; 0 = NROM
INES_MIRROR = 1 ; 0 = horizontal mirroring, 1 = vertical mirroring
INES_SRAM   = 0 ; 1 = battery backed SRAM at $6000-7FFF

; famitone2 settings
FT_BASE_ADR		= $0500	;page in the RAM used for FT2 variables, should be $xx00
FT_TEMP			= $20	;3 bytes in zeropage used by the library as a scratchpad
FT_DPCM_OFF		= $c000	;$c000..$ffc0, 64-byte steps
FT_SFX_STREAMS	= 4		;number of sound effects played at once, 1..4

FT_DPCM_ENABLE = 1		;undefine to exclude all DMC code
FT_SFX_ENABLE = 1		;undefine to exclude all sound effects code
FT_THREAD = 1			;undefine if you are calling sound effects from the same thread as the sound update call

FT_PAL_SUPPORT = 1		;undefine to exclude PAL support
FT_NTSC_SUPPORT = 1		;undefine to exclude NTSC support

.byte 'N', 'E', 'S', $1A ; ID
.byte $02 ; 16k PRG chunk count
.byte $01 ; 8k CHR chunk count
.byte INES_MIRROR | (INES_SRAM << 1) | ((INES_MAPPER & $f) << 4)
.byte (INES_MAPPER & %11110000)
.byte $0, $0, $0, $0, $0, $0, $0, $0 ; padding

;
; CHR ROM
;

.segment "TILES"
.incbin "data/whoseline.chr"

;
; vectors placed at top 6 bytes of memory area
;

.segment "VECTORS"
.word nmi
.word reset
.word irq

;
; reset routine
;

.segment "ZEROPAGE"
nmi_count:      .res 1 ; is incremented every NMI
temp:           .res 1 ; temporary variable

.segment "CODE"

.include "utils/utils.s"
.include "data/data.s"
.include "levels.s"
.include "engine/engine.s"

reset:
	sei       ; mask interrupts
	lda #0
	sta $2000 ; disable NMI
	sta $2001 ; disable rendering
	sta $4015 ; disable APU sound
	sta $4010 ; disable DMC IRQ
	lda #$40
	sta $4017 ; disable APU IRQ
	cld       ; disable decimal mode
	ldx #$FF
	txs       ; initialize stack
	; wait for first vblank
	bit $2002
	:
		bit $2002
		bpl :-
	; clear all RAM to 0
	lda #0
	ldx #0
	:
		sta $0000, X
		sta $0100, X
		sta $0200, X
		sta $0300, X
		sta $0400, X
		sta $0500, X
		sta $0600, X
		sta $0700, X
		inx
		bne :-
	; place all sprites offscreen at Y=255
	lda #255
	ldx #0
	:
		sta oam, X
		inx
		inx
		inx
		inx
		bne :-
	; wait for second vblank
	:
		bit $2002
		bpl :-
	; NES is initialized, ready to begin!
	; enable the NMI for graphical updates, and jump to our main program
	lda #%10011000
	sta $2000

	; init FamiTone
	ldx #<hoedown_music_data
	ldy #>hoedown_music_data
	lda #$80
	jsr FamiToneInit
	lda #0
	jsr FamiToneMusicPlay

	jmp main

.segment "BSS"
nmt_update: .res 256 ; nametable update entry buffer for PPU update
palette:    .res 32  ; palette buffer for PPU update

.segment "OAM"
oam: .res 256        ; sprite OAM data to be uploaded by DMA

.segment "CODE"
nmi:
	; save registers
	pha
	txa
	pha
	tya
	pha
	; prevent NMI re-entry
	lda nmi_lock
	beq :+
		jmp @nmi_end
	:
	lda #1
	sta nmi_lock
	; increment frame counter
	inc nmi_count
	;
	lda nmi_ready
	bne :+ ; nmi_ready == 0 not ready to update PPU
		jmp @ppu_update_end
	:
	cmp #2 ; nmi_ready == 2 turns rendering off
	bne :+
		lda #%00000000
		sta $2001
		ldx #0
		stx nmi_ready
		jmp @ppu_update_end
	:
	; sprite OAM DMA
	ldx #0
	stx $2003
	lda #>oam
	sta $4014
	; palettes
	lda #%10011000
	sta $2000 ; set horizontal nametable increment
	lda $2002
	lda #$3F
	sta $2006
	stx $2006 ; set PPU address to $3F00
	ldx #0
	:
		lda palette, X
		sta $2007
		inx
		cpx #32
		bcc :-
	; nametable update
	ldx #0
	cpx nmt_update_len
	bcs @scroll
	@nmt_update_loop:
		lda nmt_update, X
		sta $2006
		inx
		lda nmt_update, X
		sta $2006
		inx
		lda nmt_update, X
		sta $2007

		dex
		dex

		lda nmt_update, X ; duplicating changes to the second nametable, for easier animation
		clc
		adc #$04
		sta $2006
		inx
		lda nmt_update, X
		sta $2006
		inx
		lda nmt_update, X
		sta $2007
		
		inx
		cpx nmt_update_len
		bcc @nmt_update_loop
	lda #0
	sta nmt_update_len
@scroll:
	lda scroll_nmt
	and #%00000011 ; keep only lowest 2 bits to prevent error
	ora #%10001000
	sta $2000
	lda scroll_x
	sta $2005
	lda scroll_y
	sta $2005
	; enable rendering
	lda #%00011110
	sta $2001
	; flag PPU update complete
	ldx #0
	stx nmi_ready
@ppu_update_end:
	; if this engine had music/sound, this would be a good place to play it
	jsr FamiToneUpdate

	; unlock re-entry flag
	lda #0
	sta nmi_lock
@nmi_end:
	; restore registers and return
	pla
	tay
	pla
	tax
	pla
	rti

;
; irq
;

.segment "CODE"
irq:
	rti

.segment "CODE"

main:
	; setup 
	ldx #0
	:
		lda palette_data, X
		sta palette, X
		inx
		cpx #32
		bcc :-
	jsr setup_background

	lda #1 ; init rng
	sta seed

	draw_text prompt_text, #2, #3, #$06

	; show the screen
	jsr ppu_update

	; main loop
@loop:
	; read gamepad
	jsr gamepad_poll

	jsr game_engine

@draw:
	; keep doing this forever!
	jmp @loop

nmi_wait:
	lda nmi_count
	:
		cmp nmi_count
		beq :-

	rts

.segment "RODATA"

.include "data/hoedown.s"

prompt_text:
	.incbin "prompt.txt"

.segment "SAMPLES"
.org FT_DPCM_OFF
samples:
	.incbin "data/hoedown.dmc"